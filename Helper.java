class Helper {

    // Helper to initialize
    static void initialize(){
        // Initilaize admin
        Main.admins[0] = new Admin("joko", "ini_password_joko");
        Main.admins[1] = new Admin("dengklek", "ini_password_dengklek");
        
        // Initialize Layanan
        // Masukkan daftar-daftar layanan bersesuaian dengan soal
        Main.layanans[0] = new Layanan("Ganti Oli", 500000);
        Main.layanans[1] = new Layanan("Operasi Mobil", 2000000);
        Main.layanans[2] = new Layanan("Ketok Magic", 3000000);
        Main.layanans[3] = new Layanan("Modifikasi Mobil Racing", 5000000);
        Main.layanans[4] = new Layanan("Modifikasi Mobil Elektrik", 5500000);
    }

    // Helper function to authentication
    // Fungsi yang digunakan untuk "melakukan loop" pada iterasi admins
    static boolean authentication(String username, String password) {
        // Rubah Kode di sini
        // Return true apabila data admins sesuai dengan username dan password pada paramater
        for(Admin admin : Main.admins)
        if(admin.isMatch(username, password)){
            return true;
        }
        return false;
    }

    // Helper to format price to rupiah
    static String getFormattedPrice(int price){
        return String.format("Rp. %,d", price);
    }
}